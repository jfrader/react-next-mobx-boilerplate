
const withCss = require("@zeit/next-css");
const withPlugins = require("next-compose-plugins");

module.exports = withPlugins([withCss({cssModules: true, cssLoaderOptions: {
    importLoaders: 1,
    localIdentName: "[local]___[hash:base64:5]",
  }})])