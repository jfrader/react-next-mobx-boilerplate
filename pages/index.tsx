import * as React from "react";
import { observer, inject } from "mobx-react";
import { Fragment } from "react";
import { IAppStore } from "../src/stores/app.store";
import { WithTranslation } from "next-i18next";
import { withTranslation } from "../src/i18n";
import Test from '../src/components/TestComponent/test';

interface IHomePageProps extends WithTranslation {
  appStore: IAppStore,
  t: any
}

const HomePage = (props: IHomePageProps) => {
  return (
    <Fragment>
      <h1>{props.t('title')}: {props.appStore.title}</h1>
      <h1>{props.t('error-with-status', { statusCode: 4011})}</h1>
      <Test></Test>
    </Fragment>
  );
};

HomePage.getInitialProps = async ({ ctx, router }) => {
  return {
    namespacesRequired: ['common']
  }
};

export default inject("appStore")(observer(withTranslation('common')(HomePage)));
