import React from 'react'
import { withTranslation } from "../src/i18n";


const ErrorPage = ({ statusCode = null, t }) => (
  <p>
    {statusCode
      ? t('error-with-status', { statusCode })
      : t('error-without-status')}
  </p>
)

ErrorPage.getInitialProps = async ({ res, err }) => {
  let statusCode = null
  if (res) {
    ({ statusCode } = res)
  } else if (err) {
    ({ statusCode } = err)
  }
  return {
    namespacesRequired: ['common'],
    statusCode,
  }
}


export default withTranslation('common')(ErrorPage)