import { getSnapshot } from "mobx-state-tree";
import { Provider } from "mobx-react";
import { Container } from "next/app";
import React, { useState } from "react";
import { InitAppStore, IAppStore } from "../src/stores/app.store";
import { appWithTranslation } from "../src/i18n";
import Head from 'next/head'

interface IAppPageProps {
  isServer: boolean;
  initialState: IAppStore;
  Component: any;
  pageProps: any;
}

const AppPage = (props: IAppPageProps) => {
  const { Component, initialState, pageProps, isServer } = props;
  const [appStore] = useState(() => InitAppStore(initialState));
  return (
    <Container>
      <Head>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossOrigin="anonymous" />
      </Head>
      <Provider appStore={appStore}>

        <Component {...pageProps} />
      </Provider>
    </Container>
  );
}

AppPage.getInitialProps = async ({ Component, router, ctx }) => {
  const isServer = typeof window === "undefined";
  const appStore = InitAppStore();
  let pageProps = {};
  if (Component.getInitialProps) {
    pageProps = await Component.getInitialProps(ctx);
  }
  return {
    initialState: getSnapshot(appStore),
    isServer,
    pageProps
  };
}

export default appWithTranslation(AppPage);
