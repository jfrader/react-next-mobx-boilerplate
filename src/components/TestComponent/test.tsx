import * as React from "react";
import styles from "./test.css";

const Test = props => {
  return (
    <div className={styles.test}>
      <div>
        <h1>TESTING</h1>
      </div>
    </div>
  );
};

export default Test;
