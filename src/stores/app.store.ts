import { types, flow, applySnapshot, Instance } from "mobx-state-tree";

let AppStore = null as any;

export const AppStoreModel = types
  .model({
    title: types.string
  })
  .actions(self => {
    return {
      setTitle: flow(function*(title: string) {
        self.title = title;
        return self;
      })
    };
  });

export const InitAppStore = (snapshot: Partial<IAppStore> | null = null) => {
  AppStore = AppStoreModel.create({ title: "Boilerplate" });
  if (snapshot) {
    applySnapshot(AppStore, snapshot);
  }
  return AppStore;
};


export type IAppStore = Instance<typeof AppStoreModel>