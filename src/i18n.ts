import NextI18Next from 'next-i18next';
import * as i18nConfig from '../i18n.config';

const NextI18NextInstance = new NextI18Next(i18nConfig as any);

export default NextI18NextInstance

export const {
  appWithTranslation,
  withTranslation,
  ...instance
} = NextI18NextInstance