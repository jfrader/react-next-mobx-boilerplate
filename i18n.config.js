module.exports = {
  defaultLanguage: "en",
  otherLanguages: ["es"],
  fallbackLng: "en",
  use: ["en"],
  localeSubpaths: "all",
  defaultNS: "common"
};
